﻿using ProyectoCsharp.Persona;
using ProyectoServicioCSharp.connect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ServicioCSharp
{
    /// <summary>
    /// Descripción breve de WebService1
    /// </summary>
    [WebService(Namespace = "http://aeropuerto.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script,
        //usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {

        [WebMethod]
        public void InsertarAerolinea(string nombre, string rfc, string creacion)
        {
            Aerolinea aerol = new Aerolinea(nombre, rfc, creacion);
            aerol.registrarAerolinea();
        }

        [WebMethod]
        public void InsertarAvion(int id_aerolinea, string modelo, int capacidad, string disponibilidad)
        {
            Avion avi = new Avion(id_aerolinea, modelo, capacidad, disponibilidad);
            avi.registrarAvion();
        }

        [WebMethod]
        public void InsertarBoletos(int num_vuelo, int id_usuario, string clase, string codigo)
        {
            Boletos bol = new Boletos(num_vuelo, id_usuario, clase, codigo);
            bol.registrarBoleto();
        }

        [WebMethod]
        public void InsertarEscalas(string pais, string aeropuerto, string fecha, string hora)
        {
            Escalas esc = new Escalas(pais, aeropuerto, fecha, hora);
            esc.registrarEscalas();
        }

        [WebMethod]
        public void InsertarPersona(string nombre, string apellidos, string curp, string correo, string contra)
        {
            Persona per = new Persona(nombre, apellidos, curp, correo, contra);
            per.registrarPersona();
        }

        [WebMethod]
        public void InsertarRutas(double costoPorVuelo)
        {
            Rutas ruta = new Rutas(costoPorVuelo);
            ruta.registrarRutas();
        }

        [WebMethod]
        public void InsertarRutas_Escalas(int id_ruta, int id_escala, int contexto)
        {
            Rutas_Escalas rut = new Rutas_Escalas(id_ruta, id_escala, contexto);
            rut.registrarRutas_Escalas();
        }

        [WebMethod]
        public void InsertarVuelos(int id_avion, int id_ruta)
        {
            Vuelos vuel = new Vuelos(id_avion, id_ruta);
            vuel.registrarVuelo();
        }
    }
}
