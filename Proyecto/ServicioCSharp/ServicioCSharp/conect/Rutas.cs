﻿using Npgsql;
using ProyectoCsharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoServicioCSharp.connect
{
    public class Rutas
    {

        private int id_rutas, megaVuelo;
        private double costoPorVuelo;
        private string megaPais, megaAeropuerto, megaFecha, megaHora;

        Conexion con;

        public Rutas()
        {
        }

        public Rutas(double costoPorVuelo)
        {
            this.costoPorVuelo = costoPorVuelo;
        }

        public Rutas(int id_rutas, double costoPorVuelo)
        {
            this.id_rutas = id_rutas;
            this.costoPorVuelo = costoPorVuelo;

        }

        public Rutas(int megaVuelo, string megaPais, string megaAeropuerto,
                string megaFecha, string megaHora)
        {
            this.megaVuelo = megaVuelo;
            this.megaPais = megaPais;
            this.megaAeropuerto = megaAeropuerto;
            this.megaFecha = megaFecha;
            this.megaHora = megaHora;
        }

        public void registrarRutas()
        {
            con = new Conexion();
            string SQL = "INSERT INTO rutas(costoRuta) values(@costo)";
            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());

            cmd.Parameters.AddWithValue("costo", costoPorVuelo);
            cmd.Prepare();

            cmd.ExecuteNonQuery();
        }

        public void eliminarRutas()
        {
            con = new Conexion();
            string SQL = "DELETE FROM rutas WHERE id_ruta = @id";
            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());

            cmd.Parameters.AddWithValue("id", id_rutas);
            cmd.Prepare();

            cmd.ExecuteNonQuery();
        }

        public List<Rutas> obtenerRutas()
        {
            List<Rutas> listRutas = new List<Rutas>();
            con = new Conexion();
            string SQL = "SELECT * FROM rutas";
            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());

            NpgsqlDataReader record = cmd.ExecuteReader();
            while (record.NextResult())
            {
                listRutas.Add(new Rutas(record.GetInt32(0), record.GetDouble(1)));
            }
            return listRutas;
        }

        public List<Rutas> obtenerMegaRutas(string pais)
        {
            List<Rutas> listRutas = new List<Rutas>();
            int iterador;
            Boolean añadir = true;
            con = new Conexion();
            string SQL = "Select v.num_vuelo, e.pais, e.aeropuerto, e.fecha, e.hora "
                + "FROM vuelos v "
                + "INNER JOIN ruta_escalas r on v.id_ruta = r.id_ruta "
                + "INNER JOIN escalas e on r.id_escala = e.id_escala "
                + "WHERE e.pais = @pais";
            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());

            cmd.Parameters.AddWithValue("pais", pais);
            cmd.Prepare();

            NpgsqlDataReader record = cmd.ExecuteReader();
            while (record.NextResult())
            {
                listRutas.Add(new Rutas(record.GetInt32(0), record.GetString(1),
                    record.GetString(2), record.GetString(3), record.GetString(4)));
            }
            return listRutas;
        }
    }
}
