﻿using Npgsql;
using ProyectoCsharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoServicioCSharp.connect
{
    public class Aerolinea
    {

        public int id_aerolinea;
        public string creacion, nombre, rfc;
        Conexion con;

        public Aerolinea()
        {
        }

        public Aerolinea(string nombre)
        {
            this.nombre = nombre;
        }

        public Aerolinea(int id_aerolinea)
        {
            this.id_aerolinea = id_aerolinea;
        }

        public Aerolinea(string nombre, string rfc, string creacion)
        {
            this.nombre = nombre;
            this.rfc = rfc;
            this.creacion = creacion;
        }

        public Aerolinea(int id_aerolinea, string nombre, string rfc, string creacion)
        {
            this.id_aerolinea = id_aerolinea;
            this.nombre = nombre;
            this.rfc = rfc;
            this.creacion = creacion;
        }

        public void registrarAerolinea()
        {
            con = new Conexion();

            string SQL = "INSERT INTO aerolinea(nombre, rfc, creacion) VALUES(@nombre, @rfc, @creacion)";

            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());

            cmd.Parameters.AddWithValue("nombre", nombre);
            cmd.Parameters.AddWithValue("rfc", rfc);
            cmd.Parameters.AddWithValue("creacion", creacion);

            cmd.Prepare();
            cmd.ExecuteNonQuery();
        }

        public void modificarAerolinea()
        {
            con = new Conexion();


            string mo = "UPDATE aerolinea SET nombre=@nombre, rfc=@rfc, creacion=@creacion WHERE id_aerolinea=id";

            var cmd = new NpgsqlCommand(mo, con.ObtenerConexion());

            cmd.Parameters.AddWithValue("nombre", nombre);
            cmd.Parameters.AddWithValue("rfc", rfc);
            cmd.Parameters.AddWithValue("creacion", creacion);
            cmd.Parameters.AddWithValue("id", id_aerolinea);

            cmd.Prepare();
            cmd.ExecuteNonQuery();
        }

        public void eliminarAerolinea()
        {
            con = new Conexion();

            string SQL = "Delete From aerolinea where id_aerolinea = @id";
            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());

            cmd.Parameters.AddWithValue("id", id_aerolinea);

            cmd.Prepare();
            cmd.ExecuteNonQuery();
        }

        public List<Aerolinea> obtenerAerolineas()
        {
            List<Aerolinea> listAerolineas = new List<Aerolinea>();
            
            con = new Conexion();
            string SQL = "Select * From aerolinea";
            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());

            NpgsqlDataReader record = cmd.ExecuteReader();

            while (record.NextResult())
            {
                listAerolineas.Add(new Aerolinea(record.GetInt32(0), record.GetString(1), record.GetString(2),
                record.GetString(3)));
            }
            return listAerolineas;
        }

        public int obtenerIdPorNombre()
        {
            con = new Conexion();
            string SQL = "Select id_aerolinea From aerolinea where nombre = @nombre";
            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());

            cmd.Parameters.AddWithValue("nombre", nombre);

            NpgsqlDataReader record = cmd.ExecuteReader();

            while (record.NextResult())
            {
                id_aerolinea = record.GetInt32(0);
            }
            return id_aerolinea;
        }

        public string obtenerNombrePorId()
        {
            con = new Conexion();
            string SQL = "Select nombre From aerolinea where id_aerolinea = @id";
            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());

            cmd.Parameters.AddWithValue("id", id_aerolinea);

            cmd.Prepare();
            NpgsqlDataReader record = cmd.ExecuteReader();
            while (record.NextResult())
            {
                nombre = record.GetString(0);
            }
            return nombre;
        }
    }

}
