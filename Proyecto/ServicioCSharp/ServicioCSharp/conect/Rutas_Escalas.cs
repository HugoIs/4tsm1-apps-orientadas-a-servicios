﻿using Npgsql;
using ProyectoCsharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoServicioCSharp.connect
{
    public class Rutas_Escalas
    {

        private int id_RutaEscala, id_ruta, id_escala, contexto;

        Conexion con;

        public Rutas_Escalas()
        {
        }

        public Rutas_Escalas(int id_ruta, int id_escala, int contexto)
        {
            this.id_ruta = id_ruta;
            this.id_escala = id_escala;
            this.contexto = contexto;
        }

        public Rutas_Escalas(int id_RutaEscala, int id_ruta, int id_escala, int contexto)
        {
            this.id_RutaEscala = id_RutaEscala;
            this.id_ruta = id_ruta;
            this.id_escala = id_escala;
            this.contexto = contexto;
        }

        public void registrarRutas_Escalas()
        {
            con = new Conexion();
            string SQL = "INSERT INTO ruta_escalas(id_ruta, id_escala, contexto) values(@idr, @ide, @contexto)";
            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());

            cmd.Parameters.AddWithValue("idr", id_ruta);
            cmd.Parameters.AddWithValue("ide", id_escala);
            cmd.Parameters.AddWithValue("contexto", contexto);
            cmd.Prepare();

            cmd.ExecuteNonQuery();
        }

        public void eliminarRutas()
        {
            con = new Conexion();
            string SQL = "DELETE FROM ruta_escalas WHERE id_RutaEscala = @id";
            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());

            cmd.Parameters.AddWithValue("id", id_RutaEscala);
            cmd.Prepare();

            cmd.ExecuteNonQuery();
        }

        public List<Rutas_Escalas> obtenerRutas_Escalas()
        {
            List<Rutas_Escalas> listRutasEscalas = new List<Rutas_Escalas>();
            con = new Conexion();
            string SQL = "SELECT * FROM rutas";
            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());

            cmd.Parameters.AddWithValue("id", id_RutaEscala);

            NpgsqlDataReader record = cmd.ExecuteReader();
            while (record.NextResult())
            {
                listRutasEscalas.Add(new Rutas_Escalas(record.GetInt32(0), record.GetInt32(1),
                    record.GetInt32(2), record.GetInt32(3)));
            }
            return listRutasEscalas;
        }
    }
}
