﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ConsumirServicio
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Boton_Click1(object sender, EventArgs e)
        {
            LaHora.ServicioHugoSoapClient hora = new LaHora.ServicioHugoSoapClient();
            if (Diferencia.Text.Length == 0)
            {
                Resultado.Text = hora.Hora(0);
            }
            else
            {
                Resultado.Text = hora.Hora(int.Parse(Diferencia.Text));
            }
            
        }
    }
}