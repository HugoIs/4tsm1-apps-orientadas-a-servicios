﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoCsharp
{
    public class Conexion
    {
        String usuario;
        String contra;
        String baseDatos;
        /*
         *Asigna los valores por defecto de la conexión 
         */
        public Conexion()
        {
            usuario = "postgres";
            contra = "123";
            baseDatos = "UTTEC";
        }
        public NpgsqlConnection ObtenerConexion()
        {
            //String de conexión, asigna la conexion que se usara para conectarse a PostgreSql
            var strCon = "Host=localhost;Username=" + usuario + ";Password=" + contra + ";Database=" + baseDatos;

            //Variable que inicializa la conexión
            var con = new NpgsqlConnection(strCon);

            //Se abre la conexión a la base de datos
            con.Open();

            //Se regresa el objeto de conexión para usar en otras clases
            return con;
        }
    }
}
