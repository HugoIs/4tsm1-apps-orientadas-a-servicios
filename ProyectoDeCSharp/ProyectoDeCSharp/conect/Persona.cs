﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoCsharp.Persona
{
    public class Persona
    {
        public String nombre, curp, apellidos, rfc, edo_civil;
        private long id_persona { get; set; }

        public Persona() {
        }

        public Persona(long id_persona, String nombre, String apellidos, String curp, String rfc, String edo_civil)
        {
            this.id_persona = id_persona;
            this.nombre = nombre;
            this.apellidos = apellidos;
            this.curp = curp;
            this.rfc = rfc;
            this.edo_civil = edo_civil;
        }
        public Persona(String nombre, String apellidos, String curp, String rfc, String edo_civil)
        {
            this.nombre = nombre;
            this.apellidos = apellidos;
            this.curp = curp;
            this.rfc = rfc;
            this.edo_civil = edo_civil;
        }

        public void registrarPersona()
        {
            Conexion con = new Conexion();

            //String con la conexion de la base
            String SQL = "INSERT INTO persona(nombre, apellidos, curp, rfc, edo_civil)" +
                    "VALUES( @nombre, @apellidos, @curp, @rfc, @edo_civil)";

            //Crear un comando de para postgresql
            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());

            cmd.Parameters.AddWithValue("nombre", nombre);
            cmd.Parameters.AddWithValue("apellidos", apellidos);
            cmd.Parameters.AddWithValue("curp", curp);
            cmd.Parameters.AddWithValue("rfc", rfc);
            cmd.Parameters.AddWithValue("edo_civil", edo_civil);
            cmd.Prepare();

            cmd.ExecuteNonQuery();
        }

        public List<Persona> obtenerPersonas()
        {
            List<Persona> listPersonas = new List<Persona>();

            Conexion con = new Conexion();
            String SQL = "Select * from persona";
            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());

            NpgsqlDataReader record = cmd.ExecuteReader();

            while (record.Read())
            {
                listPersonas.Add(new Persona(record.GetInt32(0),record.GetString(1), record.GetString(2), record.GetString(3),
                        record.GetString(4), record.GetString(5)));
            }

            return listPersonas;
        }
    }
}
