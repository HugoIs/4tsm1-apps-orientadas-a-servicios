﻿using System;
using Npgsql;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using ProyectoCsharp.Persona;

namespace ProyectoDeCSharp
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class ServicioHugo : System.Web.Services.WebService
    {
        [WebMethod(Description ="Obtiene la hora actual usando como base UTC." +
            "UTCHora: Pide la diferencia con UTC (Ejemplo: México = -6)")]

        public string Hora(int UTCHora)
        {
            int hora = DateTime.UtcNow.AddHours(UTCHora).Hour;
            int min = DateTime.UtcNow.Minute;
            int seg = DateTime.UtcNow.Second;
            string minutos, segundos;
            if (min < 10)
            {
                minutos = "0" + min;
            }
            else
            {
                minutos = "" + min;
            }
            if (seg < 10)
            {
                segundos = "0" + seg;
            }
            else
            {
                segundos = "" + seg;
            }
            string resultado = hora + ":" + minutos + ":" + segundos;
            return resultado;
        }


        [WebMethod(Description = "Obtiene una lista de todas las personas en la base de datos.")]
        public List<Persona> select()
        {
            Persona personita = new Persona();
            List<Persona> resultado = personita.obtenerPersonas();
            return resultado;
        }

        [WebMethod(Description = "Registra un usuario a la base de datos.")]
        public void insert(String nombre,String apellidos,String curp,String rfc,String edo_civil)
        {
            Persona personita = new Persona(nombre, apellidos, curp, rfc, edo_civil);
            personita.registrarPersona();
        }
    }
}
