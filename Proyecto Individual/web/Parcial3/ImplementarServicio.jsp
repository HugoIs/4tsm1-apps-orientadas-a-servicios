<%-- 
    Document   : ImplementarServicio
    Created on : 3/04/2020, 06:15:48 PM
    Author     : hivgy
--%>

<%@page import="modelo.Persona"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Insertar y ver registros a la tabla persona</title>
    </head>
    <body>
        <h1>Usando servicios</h1>

        <table>
            <!--Encabezado de la tabla-->
            <tr>
                <td class="encabezado">Nombre</td>
                <td class="encabezado">Apellidos</td>
                <td class="encabezado">CURP</td>
                <td class="encabezado">RFC</td>
                <td class="encabezado">Edo. Civil</td>
            </tr>
            <!--Formulario para añadir un registro-->
            <tr>
            <form action="servicio.jsp" method="POST">
                <td><input type="text" placeholder="Jose" name="nomb"></td>
                <td><input type="text" placeholder="Villegas" name="ape"></td>
                <td><input type="text" placeholder="VAGH981117HDFGZTF02" name="curp"></td>
                <td><input type="text" placeholder="VAGH981117" name="rfc"></td>
                <td>
                    <!--Selección para evitar el ingreso de valores no deseados-->
                    <select name="civil">
                        <option value="soltero">Soltero</option>
                        <option value="casado">Casado</option>
                        <option value="libre">Unión Libre</option>
                        <option value="divorsi">Divorsiado</option>
                        <option value="viudo">Viudo</option>
                    </select>
                </td>
                <td></td>
                <td><input type="submit" value="Guardar"></td>
            </form>
        </tr>
        <%
            try {
                org.tempuri.ServicioHugo service = new org.tempuri.ServicioHugo();
                org.tempuri.ServicioHugoSoap port = service.getServicioHugoSoap();
                // TODO process result here
                org.tempuri.ArrayOfPersona result = port.select();
                for (org.tempuri.Persona p : result.getPersona()) {
        %>
        <tr>
            <!--
                obtenerPersonas() ya inicializa los valores, asi que solo
                es necesario traer esos valores y mostrarlos en la tabla
            -->
            <td><input type="text" value="<%=p.getNombre()%>" name="nomb"></td>
            <td><input type="text" value="<%=p.getApellidos()%>" name="ape"></td>
            <td><input type="text" value="<%=p.getCurp()%>" name="curp"></td>
            <td><input type="text" value="<%=p.getRfc()%>" name="rfc"></td>
            <td><input type="text" value="<%=p.getEdoCivil()%>" name="civil"></td>
        <%
                }
            } catch (Exception ex) {
                // TODO handle custom exceptions here
            }
        %>
        </tr>
    </table>
</body>
</html>
