<%--
    Documento   : Almacenar datos de persona
    Descipción  : Se utiliza el servicio de C# para registrar los datos de la persona
    Author      : Hugo Isaac Vazquez Gutierrez
--%>
<%
    try {
        org.tempuri.ServicioHugo service = new org.tempuri.ServicioHugo();
        org.tempuri.ServicioHugoSoap port = service.getServicioHugoSoap();
        //Asignar variables para no estar utilizando request.getParameter muy seguido
        String nombre = request.getParameter("nomb");
        String apellido = request.getParameter("ape");
        String curp = request.getParameter("curp");
        String rfc = request.getParameter("rfc");
        String civil = request.getParameter("civil");
        port.insert(nombre, apellido, curp, rfc, civil);
    } catch (Exception e) {
        e.printStackTrace();
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registro Correcto wooooo</title>
    </head>
    <body>
        <h1>Registro</h1>
        <%response.sendRedirect("ImplementarServicio.jsp");%>
    </body>
</html>
