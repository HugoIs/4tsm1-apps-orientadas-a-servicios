<%-- 
    Document   : ReporteEstados
    Created on : 3/03/2020, 11:47:58 AM
    Author     : Isaac
--%>

<%@page import="java.util.List"%>
<%@page import="modelo.Persona"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/style.css">
        <title>Reporte de personas de los estados</title>
    </head>
    <body>
        <%
            String[] estados = {"AS", "BS", "CL", "CS", "DF", "GT", "HG", "MC", "MS", "NL", "PL", "QR", "SL", "TC", "TL",
                "YN", "NE", "BC", "CC", "CM", "CH", "DG", "GR", "JC", "MN", "NT", "OC", "QT", "SP", "SR", "TS", "VZ", "ZS"};
            Persona p = new Persona();
            List<String> estado = p.obtenerEstados();
            List<String> genero = p.obtenerGenero();
        %>
        <table>
            <tr>
                <td>
                    Estado
                </td>
                <td>
                    Hombre
                </td>
                <td>
                    Mujer
                </td>
            </tr>
            <%
                for (int i = 0; i < estados.length; i++) {
                    int hombres = 0;
                    int mujeres = 0;
            %>
            <tr>
                <td>
                    <%=estados[i]%>
                </td>
                <td>
                    <%
                        for (int j = 0; j < estado.size(); j++) {
                            if (estado.get(j).equals(estados[i])) {
                                if (genero.get(j).equals("H")) {
                                    hombres++;
                                }
                            }
                        }
                    %>
                    <%=hombres%>
                </td>
                <td>
                    <%
                        for (int j = 0; j < estado.size(); j++) {
                            if (estado.get(j).equals(estados[i])) {
                                if (genero.get(j).equals("M")) {
                                    mujeres++;
                                }
                            }
                        }
                    %>
                    <%=mujeres%>
                </td>
            </tr>
            <%}%>
        </table>
    </body>
</html>
