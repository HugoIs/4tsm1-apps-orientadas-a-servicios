<%--
    Documento   : Almacenar datos de persona
    Descipción  : Utilizando la clase de Java Persona, se realiza conexión con
                    la base de datos y se registran los datos de la persona
    Author      : Hugo Isaac Vazquez Gutierrez
--%>

<%@page import="modelo.Persona"%>
<% 
    //Asignar variables para no estar utilizando request.getParameter muy seguido
    //En este caso no aplica, pero es una buena práctica de programación
    String nombre = request.getParameter("nomb");
    String apellido = request.getParameter("ape");
    String curp = request.getParameter("curp");
    String rfc = request.getParameter("rfc");
    String civil = request.getParameter("civil");
    
    //Crea un objeto de tipo persona
    Persona personita = new Persona();
    
    //Inicaliza los atributos de personita
    personita.setNombre(nombre);
    personita.setApellidos(apellido);
    personita.setCurp(curp);
    personita.setRfc(rfc);
    personita.setEdo_civil(civil);
    
    //Registra en la base de datos
    personita.registrarPersona();
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registro Correcto wooooo</title>
    </head>
    <body>
        <h1>Registro</h1>
        <%response.sendRedirect("crud.jsp");%>
    </body>
</html>
