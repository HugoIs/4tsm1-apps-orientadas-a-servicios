<%--
    Documento   : Eliminar un registro de una persona
    Descipción  : Utiliza la CURP ingresada anteriormente en un HTML para
                    determinar que registro eliminar
    Author      : Hugo Isaac Vazquez Gutierrez
--%>

<!--Se importa la clase persona para utilizarla en este jsp-->
<%@page import="modelo.Persona"%>

<% 
    //Solicita el parametro id al formulario de la página anterior
    Long id_persona = Long.parseLong(request.getParameter("id"));
    String nombre = request.getParameter("nomb");
    String apellido = request.getParameter("ape");
    String curp = request.getParameter("curp");
    String rfc = request.getParameter("rfc");
    String civil = request.getParameter("civil");
    
    //Crea un objeto de tipo persona y asgina su identificador
    Persona personita = new Persona();
    personita.setId_persona(id_persona);
    personita.setNombre(nombre);
    personita.setApellidos(apellido);
    personita.setCurp(curp);
    personita.setRfc(rfc);
    personita.setEdo_civil(civil);
    
    //LLama al metodo para eliminar la persona
    personita.eliminarPersona();
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Redirigiendo...</title>
    </head>
    <body>
        <!--Mensaje que se muestra en la página, en caso que la página tarde en cargar-->
        <h1>Eliminando, Por favor espera</h1>
        <!--Redirecciona de regreso al formulario de eliminar-->
        <%response.sendRedirect("crud.jsp");%>
    </body>
</html>