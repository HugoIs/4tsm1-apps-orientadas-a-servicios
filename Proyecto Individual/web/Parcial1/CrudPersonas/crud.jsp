<%@page import="modelo.Persona"%>
<!--
    Documento   : Gestión de la clase persona
    Descipción  : Tabla que se encarga de agregar, modificar, eliminar y leer los
                    datos de la tabla persona
    Author      : Hugo Isaac Vazquez Gutierrez
-->
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!--Llamar a la hoja de estilo style.css-->
        <link rel="stylesheet" href="css/style.css">
        <title>Gestión de Crud</title>
    </head>
    <body>
        <h1>Personas</h1>
        <table>
            <!--Encabezado de la tabla-->
            <tr>
                <td class="encabezado">Nombre</td>
                <td class="encabezado">Apellidos</td>
                <td class="encabezado">CURP</td>
                <td class="encabezado">RFC</td>
                <td class="encabezado">Edo. Civil</td>
            </tr>
            <!--Formulario para añadir un registro-->
            <tr>
            <form action="guardar.jsp" method="POST">
                <td><input type="text" placeholder="Jose" name="nomb"></td>
                <td><input type="text" placeholder="Villegas" name="ape"></td>
                <td><input type="text" placeholder="VAGH981117HDFGZTF02" name="curp"></td>
                <td><input type="text" placeholder="VAGH981117" name="rfc"></td>
                <td>
                    <!--Selección para evitar el ingreso de valores no deseados-->
                    <select name="civil">
                        <option value="soltero">Soltero</option>
                        <option value="casado">Casado</option>
                        <option value="libre">Unión Libre</option>
                        <option value="divorsi">Divorsiado</option>
                        <option value="viudo">Viudo</option>
                    </select>
                </td>
                <td></td>
                <td><input type="submit" value="Guardar"></td>
            </form>
        </tr>
        <!--Siguiente columna-->
        <%
            for (Persona p : new Persona().obtenerPersonas()) {
        %>
        <tr>

            <!--
                obtenerPersonas() ya inicializa los valores, asi que solo
                es necesario traer esos valores y mostrarlos en la tabla
            -->
            <td><input type="text" value="<%=p.getNombre()%>" name="nomb"></td>
            <td><input type="text" value="<%=p.getApellidos()%>" name="ape"></td>
            <td><input type="text" value="<%=p.getCurp()%>" name="curp"></td>
            <td><input type="text" value="<%=p.getRfc()%>" name="rfc"></td>
            <td>
                <select name="civil">
                    <option value="soltero"><%=p.getEdo_civil()%></option>
                    <option value="soltero">Soltero</option>
                    <option value="casado">Casado</option>
                    <option value="libre">Unión Libre</option>
                    <option value="divorsi">Divorsiado</option>
                    <option value="viudo">Viudo</option>
                </select>
            </td>
            <!--Metodo para obtener la edad de la persona, usando su curp-->
            <td><%=p.obtenerEdad()%></td>
            <td>
                <input type="hidden" value="<%=p.getId_persona()%>" name="id">
                <input type="submit" value="Modificar">
            </td>
            <td>
                <!--Formulario para eliminar un registro-->
                <form action="borrar.jsp" method="POST">
                    <input type="hidden" value="<%=p.getId_persona()%>" name="id">
                    <input type="submit" value="Eliminar">
                </form>
                <%}%>
            </td>
        </tr>
    </table>
</body>
</html>
