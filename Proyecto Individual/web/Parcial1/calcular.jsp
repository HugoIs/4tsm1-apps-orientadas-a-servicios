
<%@page import="primerPaquete.Cuadrado"%>
<%@page import="primerPaquete.Rectangulo"%>
<%@page import="primerPaquete.Circulo"%>
<%@page import="primerPaquete.Poligono"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Resultado</title>
        <link rel="stylesheet" href="../css/style.css">
    </head>
    <body>
        <%
            double uno = Double.parseDouble(request.getParameter("1"));
            double dos;
            double area = -1;
            double perimetro = -1;
            String valor = request.getParameter("Figura");

            if (valor.equals("Cuadrado")) {
                Cuadrado cuadrito = new Cuadrado(uno);
                if (request.getParameter("area") != null) {
                    area = cuadrito.obtenerArea();
                }
                if (request.getParameter("perim") != null) {
                    perimetro = cuadrito.obtenerPerimetro();
                }
            } else if (valor.equals("Rectangulo")) {
                dos = Double.parseDouble(request.getParameter("2"));
                Rectangulo rectangulito = new Rectangulo(uno, dos);
                if (request.getParameter("area") != null) {
                    area = rectangulito.obtenerArea();
                }
                if (request.getParameter("perim") != null) {
                    perimetro = rectangulito.obtenerPerimetro();
                }
            } else if (valor.equals("Poligono")) {
                dos = Double.parseDouble(request.getParameter("2"));
                Poligono poligonito = new Poligono(dos, uno);
                if (request.getParameter("area") != null) {
                    area = poligonito.obtenerArea();
                }
                if (request.getParameter("perim") != null) {
                    perimetro = poligonito.obtenerPerimetro();
                }
            } else if (valor.equals("Circulo")) {
                Circulo circulito = new Circulo(uno);
                if (request.getParameter("area") != null) {
                    area = circulito.obtenerArea();
                }
                if (request.getParameter("perim") != null) {
                    perimetro = circulito.obtenerPerimetro();
                }
            }
        %>
        <h1>Resultado</h1>
        <h1 id = "area">Area: <%=area%></h1>
        <h1 id = "perim">Perimetro: <%=perimetro%></h1>
        <script>
            onload = function () {
                if (<%=area%> === -1.0) {
                    document.getElementById("area").style.display = "none";
                }
                if (<%=perimetro%> === -1.0) {
                    document.getElementById("perim").style.display = "none";
                }
            };
        </script>
    </body>
</html>
